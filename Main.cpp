/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Runner (https://gitlab.com/DesQ/DesQUtils/Runner)
 * DesQ Runner is a simple app launcher for the the DesQ Project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

// Local Headers
#include "Global.hpp"
#include "Runner.hpp"

#include <signal.h>
#include <DFUtils.hpp>

#include <desq/Utils.hpp>
#include <desq/desq-config.h>

#include <wayqt/WayQtUtils.hpp>
#include <wayqt/Registry.hpp>

DFL::Settings *runSett    = nullptr;
DFL::Settings *netSett    = nullptr;
WQt::Registry *wlRegistry = nullptr;

int main( int argc, char **argv ) {
    QDir cache( DFL::XDG::xdgCacheHome() );

    DFL::log = fopen( cache.filePath( "DesQ/Runner.log" ).toLocal8Bit().data(), "a" );
    qInstallMessageHandler( DFL::Logger );

    qDebug() << "------------------------------------------------------------------------";
    qDebug() << "DesQ Runner started at" << QDateTime::currentDateTime().toString( "yyyyMMddThhmmss" ).toUtf8().constData();
    qDebug() << "------------------------------------------------------------------------";

    qputenv( "QT_WAYLAND_USE_BYPASSWINDOWMANAGERHINT", QByteArrayLiteral( "1" ) );

    DFL::Application app( argc, argv );

    app.setOrganizationName( "DesQ" );
    app.setApplicationName( "Runner" );
    app.setApplicationVersion( PROJECT_VERSION );
    app.setQuitOnLastWindowClosed( false );

    app.interceptSignal( SIGSEGV, true );
    app.interceptSignal( SIGABRT, true );

    if ( app.isRunning() ) {
        app.messageServer( "toggle" );
        return 0;
    }

    if ( not app.lockApplication() ) {
        qWarning() << "Failed to lock instance. Multiple isntances may start.";
    }

    /* Settings Instances */
    runSett = DesQ::Utils::initializeDesQSettings( "Runner", "Runner" );
    netSett = new DFL::Settings( "DesQ", "Network", "Network" );

    wlRegistry = new WQt::Registry( WQt::Wayland::display() );
    wlRegistry->setup();

    DesQ::Runner::UI *runner = new DesQ::Runner::UI();

    runner->startTray();

    QObject::connect( &app, &DFL::Application::messageFromClient, runner, &DesQ::Runner::UI::handleMessage );

    return app.exec();
}
