/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Runner (https://gitlab.com/DesQ/DesQUtils/Runner)
 * DesQ Runner is a simple app launcher for the the DesQ Project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

/**
 * TODO
 * Implement Ranked Fuzzy Search
 * https://github.com/forrestthewoods/lib_fts/blob/master/code/fts_fuzzy_match.h
 */

#pragma once

#include "Global.hpp"

namespace DesQ {
    namespace Runner {
        class AppSearch;

        typedef struct _DesktopApp {
            QString name;
            QString genericName;
            QString comment;
            QString exec;
        } DesktopApp;
    }
}

class DesQ::Runner::AppSearch : public QObject {
    Q_OBJECT;

    public:
        AppSearch();

        /** Return all the desktops */
        DFL::AppsList desktops();
        QStringList cliApps();

        /** Fuzzy Search for @app. */
        DFL::AppsList searchDesktops( QString app );
        QStringList searchCLIs( QString app );

        /** Load the list of apps and clis */
        void loadDatabase();

    private:

        /**
         * Parse the desktop files in all the folders of @desktopPaths
         * If the optional @path is not empty, parse that desktop or
         * all the desktops only in that folder.
         */
        void listDesktops( QString path = QString() );
        void parseDesktop( QString path );

        /** List the CLI apps in @PATH, or optionally in @path */
        void listCLIs( QString path = QString() );

        QHash<QString, DFL::XDG::DesktopFile> desktopsList;
        QStringList cliAppsList;

        /** Location of the CLI apps */
        QStringList canonPaths;

        /** Location of the Desktop Apps */
        const QStringList desktopPaths = {
            QDir::home().filePath( ".local/share/flatpak/exports/share/applications/" ),

            QDir::home().filePath( ".local/share/applications/" ),
            QDir::home().filePath( ".local/share/applications/wine/" ),
            QDir::home().filePath( ".local/share/games/" ),

            "/var/lib/flatpak/exports/share/applications/",
            "/usr/local/share/applications/",
            "/usr/local/share/games/",

            "/usr/share/applications/",
            "/usr/share/games/"
        };

    Q_SIGNALS:
        void desktopsReady();
        void clisReady();

        /** A desktop file was added/removed/modified */
        void updateDesktop( QString path );

        /** A cli file was added/removed */
        void updateCli( QString path );
};
