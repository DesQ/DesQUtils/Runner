/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Runner (https://gitlab.com/DesQ/DesQUtils/Runner)
 * DesQ Runner is a simple app launcher for the the DesQ Project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <QtConcurrent>

#include "AppSearch.hpp"

#include <DFInotify.hpp>
#include <desq/Utils.hpp>

DesQ::Runner::AppSearch::AppSearch() : QObject() {
    /** Get our $PATH */
    QStringList PATHS = qEnvironmentVariable( "PATH" ).split( ":" );

    /**
     * Populate
     * Remove symlinks resulting from user-merge
     */
    for ( QString path: PATHS ) {
        if ( path.endsWith( "/" ) ) {
            path.chop( 1 );
        }

        canonPaths << QFileInfo( path ).canonicalFilePath();
    }

    /** Remove duplicates, if any, due to user-merge */
    canonPaths.removeDuplicates();

    DFL::Inotify *fsw = new DFL::Inotify();

    connect(
        fsw, &DFL::Inotify::nodeCreated, [ = ]( QString path ) {
            /** To differentiate between cli and desktop apps */
            if ( canonPaths.contains( QFileInfo( path ).path() ) ) {
                emit updateCli( path );
            }

            else {
                emit updateDesktop( path );
            }
        }
    );

    connect(
        fsw, &DFL::Inotify::nodeChanged, [ = ]( QString path ) {
            if ( desktopPaths.contains( QFileInfo( path ).path() + "/" ) ) {
                emit updateDesktop( path );
            }

            /** We don't care if CLI apps get modified */
        }
    );

    connect(
        fsw, &DFL::Inotify::nodeDeleted, [ = ]( QString path ) {
            if ( canonPaths.contains( QFileInfo( path ).path() ) ) {
                emit updateCli( path );
            }

            else {
                desktopsList.remove( QFileInfo( path ).fileName() );
                emit updateDesktop( path );
            }
        }
    );

    /** Path of binaries */
    for ( QString path: canonPaths ) {
        if ( not QFileInfo( path ).isDir() ) {
            continue;
        }

        fsw->addWatch( path, DFL::Inotify::PathOnly );
    }

    /** Path of desktops */
    for ( QString path: desktopPaths ) {
        if ( not QFileInfo( path ).isDir() ) {
            continue;
        }

        fsw->addWatch( path, DFL::Inotify::PathOnly );
    }

    fsw->startWatch();
}


DFL::AppsList DesQ::Runner::AppSearch::desktops() {
    return desktopsList.values();
}


QStringList DesQ::Runner::AppSearch::cliApps() {
    return cliAppsList;
}


DFL::AppsList DesQ::Runner::AppSearch::searchDesktops( QString app ) {
    DFL::AppsList matches;

    // QRegularExpression re( QString( "[%1]+" ).arg( app ) );
    QString re( app.toLower() );

    for ( DFL::XDG::DesktopFile desktop: desktopsList.values() ) {
        if ( desktop.name().contains( re, Qt::CaseInsensitive ) ) {
            matches << desktop;
            continue;
        }

        if ( desktop.genericName().contains( re, Qt::CaseInsensitive ) ) {
            matches << desktop;
            continue;
        }

        if ( desktop.description().contains( re, Qt::CaseInsensitive ) ) {
            matches << desktop;
            continue;
        }

        if ( desktop.executable().contains( re, Qt::CaseInsensitive ) ) {
            matches << desktop;
            continue;
        }
    }

    return matches;
}


QStringList DesQ::Runner::AppSearch::searchCLIs( QString app ) {
    /** Split at whitespace of any kind */
    QStringList parts = app.split( QRegularExpression( "\\s+" ), Qt::SkipEmptyParts );
    QStringList matches;
    QString     re( parts[ 0 ].toLower() );

    for ( QString cliApp: cliAppsList ) {
        if ( cliApp.contains( re, Qt::CaseInsensitive ) ) {
            matches << cliApp;
        }
    }

    /**
     * TODO: Sort the matches. For example.
     * 1. When the query is '> python', the first match should be
     *    /usr/bin/python or /bin/python, instead of /usr/bin/depythontex
     * 2. When the query is '> julia', the first match should be
     *    /opt/julia/bin/julia instead of /usr/bin/julia, if /opt... has
     *    higher preference in $PATH than /bin or /usr/bin
     * $PATH is first come, first serve.
     * Full matches have higher precedence to partial matches.
     * /usr/bin/python > /bin/depythontex even if /bin has higher
     * preference in $PATH.
     * Honour usr-merge.
     */

    return matches;
}


void DesQ::Runner::AppSearch::loadDatabase() {
    QFuture<void> future1 = QtConcurrent::run(
        [ = ] () {
            listDesktops();
        }
    );

    QFuture<void> future2 = QtConcurrent::run(
        [ = ] () {
            listCLIs();
        }
    );

    Q_UNUSED( future1 );
    Q_UNUSED( future2 );
}


void DesQ::Runner::AppSearch::listDesktops( QString path ) {
    /** List desktops from a specific location: @path */
    if ( path.length() ) {
        QDir appDir( path );
        appDir.setFilter( QDir::Files );
        appDir.setNameFilters( { "*.desktop" } );

        for ( QString app: appDir.entryList() ) {
            parseDesktop( app );
        }
    }

    /** List desktops from all locations */
    else {
        for ( QString path: desktopPaths ) {
            QDir appDir( path );
            appDir.setFilter( QDir::Files );
            appDir.setNameFilters( { "*.desktop" } );

            for ( QString app: appDir.entryList() ) {
                /** This application has already been processed at a higher ranked directory */
                if ( desktopsList.contains( app ) ) {
                    continue;
                }

                parseDesktop( app );
            }
        }
    }

    emit desktopsReady();
}


void DesQ::Runner::AppSearch::parseDesktop( QString path ) {
    // if ( not DFL::XDG::DesktopFile::exists( desktopFile ) ) {
    //     desktopsList.remove( QFileInfo( path ).fileName() );
    //     return;
    // }

    DFL::XDG::DesktopFile desktopFile( QFileInfo( path ).fileName() );

    if ( not desktopFile.isValid() ) {
        return;
    }

    if ( desktopFile.type() != DFL::XDG::DesktopFile::Type::Application ) {
        return;
    }

    if ( desktopFile.visible() ) {
        desktopsList[ QFileInfo( path ).fileName() ] = desktopFile;
    }

    else {
        desktopsList.remove( QFileInfo( path ).fileName() );
    }
}


void DesQ::Runner::AppSearch::listCLIs( QString path ) {
    if ( path.length() ) {
        QDirIterator it( path, QDir::Files | QDir::Executable );
        while ( it.hasNext() ) {
            cliAppsList << it.next();
        }

        return;
    }

    for ( QString path: canonPaths ) {
        QDirIterator it( path, QDir::Files | QDir::Executable );
        while ( it.hasNext() ) {
            cliAppsList << it.next();
        }
    }

    emit clisReady();
}
