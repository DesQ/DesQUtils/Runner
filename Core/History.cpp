/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Runner (https://gitlab.com/DesQ/DesQUtils/Runner)
 * DesQ Runner is a simple app launcher for the the DesQ Project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "History.hpp"

DesQ::Runner::History::History() {
    entries      = runSett->value( "Session::History" );
    curPos       = entries.count();
    maxHistItems = runSett->value( "MaxHistoryItems" );
}


QString DesQ::Runner::History::older() {
    if ( not maxHistItems ) {
        return QString();
    }

    if ( curPos ) {
        curPos--;
    }

    return entries.value( curPos );
}


QString DesQ::Runner::History::newer() {
    if ( not maxHistItems ) {
        return QString();
    }

    if ( curPos < entries.count() ) {
        curPos++;
    }

    return entries.value( curPos );
}


void DesQ::Runner::History::store( QString entry ) {
    if ( entries.contains( entry ) ) {
        entries.removeAll( entry );
    }

    entries << entry;

    if ( not maxHistItems ) {
        runSett->setValue( "Session::History", QStringList() );
        return;
    }

    else if ( maxHistItems > 0 ) {
        while ( entries.count() > maxHistItems ) {
            entries.removeFirst();
        }
    }

    runSett->setValue( "Session::History", entries );
    curPos = entries.count();
}


void DesQ::Runner::History::resetPosition() {
    curPos = entries.count();
}
