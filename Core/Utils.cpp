/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Runner (https://gitlab.com/DesQ/DesQUtils/Runner)
 * DesQ Runner is a simple app launcher for the the DesQ Project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "Utils.hpp"
#include <desq/Utils.hpp>

static bool isDesktop( QString app ) {
    if ( app.endsWith( ".desktop" ) ) {
        return true;
    }

    return false;
}


bool isCli( QString app ) {
    QFileInfo execInfo( app );

    return (execInfo.isFile() and execInfo.isExecutable() );
}


bool launchApp( QString app, QStringList args, bool runInTerm ) {
    /** Prepare the proxy */
    DesQ::Environment proxyEnviron = DesQ::Utils::getNetworkProxy();

    QProcessEnvironment pe( QProcessEnvironment::systemEnvironment() );

    for ( QString key: proxyEnviron.keys() ) {
        pe.insert( key, proxyEnviron.value( key ) );
    }

    /** If we're given a desktop file */
    if ( isDesktop( app ) ) {
        DFL::XDG::DesktopFile desktop( app );
        return desktop.startApplication( args, QString(), pe );
    }

    /** If app is an executable */
    if ( runInTerm or isCli( app ) ) {
        QProcess proc;
        proc.setProgram( app );
        proc.setArguments( args );
        proc.setWorkingDirectory( QDir::homePath() );
        proc.setProcessEnvironment( pe );

        return proc.startDetached();
    }

    return false;
}
