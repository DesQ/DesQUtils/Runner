/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Runner (https://gitlab.com/DesQ/DesQUtils/Runner)
 * DesQ Runner is a simple app launcher for the the DesQ Project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

/**
 * TODO
 * Implement Ranked Fuzzy Search
 * https://github.com/forrestthewoods/lib_fts/blob/master/code/fts_fuzzy_match.h
 */

#pragma once

#include "Global.hpp"

/**
 * Launch an app mindful of network preferences
 * arg1: desktop name or executable path
 * arg2: arguments to be passed
 * arg3: run in terminal?
 */
bool launchApp( QString, QStringList, bool );

/**
 * Check if the given path @app is an executable file or not.
 */
bool isCli( QString app );
