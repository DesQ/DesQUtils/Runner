/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Runner (https://gitlab.com/DesQ/DesQUtils/Runner)
 * DesQ Runner is a simple app launcher for the the DesQ Project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "Global.hpp"
#include "Runner.hpp"
#include "AppSearch.hpp"
#include "History.hpp"
#include "Core/Utils.hpp"

#include <wayqt/WayQtUtils.hpp>
#include <wayqt/Registry.hpp>
#include <wayqt/LayerShell.hpp>


void OptionsDelegate::paint( QPainter *painter, const QStyleOptionViewItem& option, const QModelIndex& index ) const {
    if ( index.column() != 0 ) {
        QStyledItemDelegate::paint( painter, option, index );
    }

    else {
        QRect optionRect( option.rect );

        // Get icon size
        QSize iconSize( option.decorationSize );

        QString text = index.data( Qt::DisplayRole ).toString();
        QString base;

        if ( text.startsWith( "/" ) ) {
            base = text.split( "/" ).last();
        }

        else {
            /** Gui app */
            base = text;
            /** Get the app description */
            text = index.data( Qt::UserRole + 2 ).toString();
        }

        QIcon rawIcon = index.data( Qt::DecorationRole ).value<QIcon>();

        if ( rawIcon.isNull() or not rawIcon.actualSize( iconSize ).isValid() ) {
            rawIcon = QIcon::fromTheme( "application-x-executable" );
        }

        QPixmap icon;

        if ( option.state & QStyle::State_Selected ) {
            icon = rawIcon.pixmap( iconSize, QIcon::Selected );
        }

        else if ( option.state & QStyle::State_MouseOver ) {
            icon = rawIcon.pixmap( iconSize, QIcon::Active );
        }

        else {
            icon = rawIcon.pixmap( iconSize, QIcon::Normal );
        }

        if ( icon.size() != iconSize ) {
            icon = icon.scaled( iconSize, Qt::KeepAspectRatio, Qt::SmoothTransformation );
        }

        QSize iSize( icon.size() );

        // Icon padding
        qreal padding = iconSize.width() * 0.1;

        QRectF iconRect;

        // Original X + Image Left Border + Width to make the ima
        iconRect.setX( optionRect.x() + padding + (iconSize.width() - iSize.width() ) / 2 );
        // Original Y + Image Top Border + Height to make the image center of the icon rectangle
        iconRect.setY( optionRect.y() + (optionRect.height() - iSize.height() ) / 2 );
        // Icon Size
        iconRect.setSize( iSize );

        /* Selection rectangle */
        painter->save();

        /* Selection painter settings */
        painter->setPen( QPen( Qt::NoPen ) );

        if ( (option.state & QStyle::State_Selected) and (option.state & QStyle::State_MouseOver) ) {
            painter->setBrush( option.palette.color( QPalette::Highlight ).darker( 115 ) );
        }

        else if ( option.state & QStyle::State_Selected ) {
            painter->setBrush( option.palette.color( QPalette::Highlight ) );
        }

        else if ( option.state & QStyle::State_MouseOver ) {
            painter->setBrush( option.palette.color( QPalette::Highlight ).darker( 130 ) );
        }

        else {
            painter->setBrush( QBrush( Qt::transparent ) );
        }

        /* Selection rectangle */
        painter->drawRect( optionRect );

        /* Focus Rectangle - In our case focus under line */
        if ( option.state & QStyle::State_HasFocus ) {
            painter->setBrush( Qt::NoBrush );
            QPoint bl = optionRect.bottomLeft() + QPoint( 7, -1 );
            QPoint br = optionRect.bottomRight() - QPoint( 7, 1 );

            QLinearGradient hLine( bl, br );

            hLine.setColorAt( 0,   Qt::transparent );
            hLine.setColorAt( 0.3, option.palette.color( QPalette::BrightText ) );
            hLine.setColorAt( 0.7, option.palette.color( QPalette::BrightText ) );
            hLine.setColorAt( 1,   Qt::transparent );

            painter->setPen( QPen( QBrush( hLine ), 1 ) );
            painter->drawLine( bl, br );
        }

        painter->restore();

        // Paint Icon
        painter->save();
        painter->setRenderHint( QPainter::Antialiasing, true );
        painter->drawPixmap( iconRect.toRect(), icon );
        painter->restore();

        painter->save();
        // Text Painter Settings
        painter->setRenderHints( QPainter::Antialiasing | QPainter::TextAntialiasing );

        QFont baseFont = option.font;
        baseFont.setPixelSize( baseFont.pixelSize() - 3 );

        // Bold text for focussed item
        if ( option.state & QStyle::State_HasFocus ) {
            baseFont.setWeight( QFont::Bold );
        }

        if ( (option.state & QStyle::State_MouseOver) or (option.state & QStyle::State_Selected) ) {
            painter->setPen( option.palette.color( QPalette::HighlightedText ) );
        }

        else {
            painter->setPen( option.palette.color( QPalette::WindowText ) );
        }

        QRectF textRect;

        // Original X + Image Width + Image-padding
        textRect.setX( optionRect.x() + iconSize.width() + 2 * padding );
        // Original Width - Image Left Border - Image Width - Image Text Gap - Text Right Border
        textRect.setWidth( optionRect.width() - iconSize.width() - 3 * padding );

        /** Draw only the main text */
        if ( iconSize.width() < 32 ) {
            /* Font Mentrics for elided text */
            QFontMetricsF fm( baseFont );

            // Vertical Centering, so don't bother
            textRect.setY( optionRect.y() );

            // Original Height
            textRect.setHeight( optionRect.height() );

            QString fullText = fm.elidedText( base + " " + text, Qt::ElideRight, textRect.width() );

            painter->setFont( baseFont );
            painter->drawText( textRect, Qt::AlignVCenter, fullText );
        }

        /** Draw extra text as well */
        else {
            QTextDocument doc;
            doc.setDefaultFont( baseFont );
            doc.setHtml( QString( "%1<br><small>%2</small>" ).arg( base, text ) );

            QTextOption txtOpt;
            txtOpt.setWrapMode( QTextOption::NoWrap );
            doc.setDefaultTextOption( txtOpt );

            /** Elide text */
            if ( doc.size().width() > textRect.width() ) {
                // Elide text
                QTextCursor cursor( &doc );
                cursor.movePosition( QTextCursor::End );

                const QString elidedPostfix = "...";
                QFontMetricsF metric( option.font );
                int           postfixWidth = metric.horizontalAdvance( elidedPostfix ) * 1.25;

                while ( doc.size().width() > textRect.width() - postfixWidth ) {
                    cursor.deletePreviousChar();
                    doc.adjustSize();
                }

                cursor.insertText( elidedPostfix );
            }

            painter->translate( textRect.x(), optionRect.y() + (iSize.height() - doc.size().height() ) / 2.0 );
            doc.drawContents( painter );
        }

        painter->restore();
    }
}


/**
 * This code is contributed by eyllanesc (https://stackoverflow.com/users/6622587/eyllanesc)
 * In Qt, sometimes there are issues with text eliding in QListView::ListMode. When using
 * uniformItemSizes, the item does not utilize the entire space of the grid available to it.
 * Setting the correct sizeHint based on the gridSize, ensures that complete space is used.
 **/
QSize OptionsDelegate::sizeHint( const QStyleOptionViewItem&, const QModelIndex& ) const {
    int iSize = (int)runSett->value( "IconSize" );

    /* Tile View */
    return QSize( qMax( 420, qApp->primaryScreen()->size().width() / 3 - 30 ), iSize );
}


OptionsView::OptionsView( QWidget *parent ) : QListView( parent ) {
    int iSize = (int)runSett->value( "IconSize" );

    setIconSize( QSize( iSize, iSize ) );
    setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
    setVerticalScrollBarPolicy( Qt::ScrollBarAlwaysOff );

    setItemDelegate( new OptionsDelegate() );
}


void OptionsView::focusInEvent( QFocusEvent *fEvent ) {
    if ( fEvent->gotFocus() ) {
        if ( selectionModel()->hasSelection() == false ) {
            setCurrentIndex( model()->index( 0, 0 ) );
            fEvent->accept();
        }

        return;
    }

    QListView::focusInEvent( fEvent );
}


DesQ::Runner::UI::UI() : QWidget() {
    /** App Searcher */
    searcher = new DesQ::Runner::AppSearch();

    /** How many items should be visible */
    mIconSize       = runSett->value( "IconSize" );
    maxVisibleItems = runSett->value( "MaxVisibleItems" );

    /** Search History */
    history = new DesQ::Runner::History();

    /** Timer to delay search start */
    delayTimer = new QBasicTimer();

    /** Terminal Settings */
    terminal     = ( QString )runSett->value( "Terminal::AppExec" );
    termSwitches = runSett->value( "Terminal::ExecSwitches" );

    /** QLineEdit's bad search palette */
    bad = qApp->palette();
    bad.setColor( QPalette::Text, Qt::darkRed );

    createUI();
    makeConnections();
    setupWindowProperties();
}


DesQ::Runner::UI::~UI() {
    delete clisModel;
    delete appsModel;
    delete proxyModel;
    delete currentMdl;
    delete cmdEdit;
    delete options;
    delete searcher;
    delete history;

    delayTimer->stop();
    delete delayTimer;

    delete cls;
}


void DesQ::Runner::UI::startTray() {
    QSystemTrayIcon *icon = new QSystemTrayIcon( QIcon::fromTheme( "desq-runner", QIcon( ":/desq-runner.png" ) ), this );

    icon->show();

    connect(
        icon, &QSystemTrayIcon::activated, [ = ]( QSystemTrayIcon::ActivationReason reason ) {
            switch ( reason ) {
                case QSystemTrayIcon::Trigger: {
                    if ( isVisible() ) {
                        hide();
                    }

                    else {
                        show();
                    }

                    break;
                }

                case QSystemTrayIcon::Context: {
                    break;
                }

                case QSystemTrayIcon::MiddleClick: {
                    qApp->quit();
                    break;
                }

                case QSystemTrayIcon::DoubleClick: {
                    break;
                }

                case QSystemTrayIcon::Unknown: {
                    break;
                }
            }
        }
    );
}


void DesQ::Runner::UI::createUI() {
    cmdEdit = new QLineEdit();
    cmdEdit->setPlaceholderText( "Type to search..." );
    cmdEdit->setFixedHeight( mIconSize );

    options    = new OptionsView( this );
    proxyModel = new QSortFilterProxyModel( options );
    proxyModel->setFilterCaseSensitivity( Qt::CaseInsensitive );

    options->setModel( proxyModel );

    /** Source models */
    appsModel = new QStandardItemModel();
    clisModel = new QStandardItemModel();

    /** Font Settings */
    QFont f( font() );
    f.setPixelSize( (int)runSett->value( "FontSize" ) );

    cmdEdit->setFont( f );
    options->setFont( f );

    QVBoxLayout *lyt = new QVBoxLayout();
    lyt->setContentsMargins( QMargins( 10, 10, 10, 10 ) );
    lyt->setSpacing( 5 );

    lyt->addWidget( cmdEdit );
    lyt->addWidget( options );

    setLayout( lyt );

    options->hide();

    QPalette pltt( palette() );
    pltt.setColor( QPalette::Base, Qt::transparent );
    setPalette( pltt );

    setStyleSheet(
        "QWidget{"
        "   border: none;"
        "   border-radius: 5px;"
        "   background-color: transparent;"
        "}"
        "QLineEdit {"
        "   border: none;"
        "   background-color: transparent;"
        "}"
        "QListView {"
        "   border: none;"
        "   border-radius: 0px;"
        "   border-top: 1px solid gray;"
        "   background-color: transparent;"
        "}"
    );
}


void DesQ::Runner::UI::makeConnections() {
    connect(
        searcher, &DesQ::Runner::AppSearch::desktopsReady, [ = ] () {
            for ( DFL::XDG::DesktopFile desktop: searcher->desktops() ) {
                QStandardItem *item = new QStandardItem();

                item->setIcon( QIcon::fromTheme( desktop.icon() ) );
                item->setText( desktop.name() );
                item->setData( desktop.genericName(),    Qt::UserRole + 1 );
                item->setData( desktop.description(),    Qt::UserRole + 2 );
                item->setData( desktop.desktopFileUrl(), Qt::UserRole + 3 );

                QString searchData = desktop.name();
                searchData        += "\n" + desktop.genericName();
                searchData        += "\n" + desktop.description();
                searchData        += "\n" + desktop.desktopFileUrl();

                /** We will use this column for search */
                item->setData( searchData, Qt::UserRole + 4 );

                appsModel->appendRow( item );
                appsMap[ desktop.desktopName() ] = item;

                qApp->processEvents();
            }
        }
    );

    connect(
        searcher, &DesQ::Runner::AppSearch::clisReady, [ = ] () {
            for ( QString cli: searcher->cliApps() ) {
                QStandardItem *item = new QStandardItem();

                item->setIcon( QIcon::fromTheme( "utilities-terminal" ) );
                item->setText( cli );

                clisModel->appendRow( item );
                clisMap[ cli ] = item;

                qApp->processEvents();
            }
        }
    );

    connect(
        searcher, &DesQ::Runner::AppSearch::updateCli, [ = ] ( QString cli ) {
            /** File info */
            QFileInfo cliInfo( cli );

            /** This exists: it's to be added */
            if ( cliInfo.exists() && cliInfo.isExecutable() ) {
                /** Add if it is not been already added */
                if ( clisMap.contains( cli ) == false ) {
                    QStandardItem *item = new QStandardItem();

                    item->setIcon( QIcon::fromTheme( "utilities-terminal" ) );
                    item->setText( cli );

                    clisModel->appendRow( item );
                    clisMap[ cli ] = item;

                    qApp->processEvents();
                }
            }

            /** File does not exist: so it's to be removed */
            else {
                if ( clisMap.contains( cli ) ) {
                    clisModel->removeRow( clisMap[ cli ]->row() );
                    clisMap.remove( cli );
                }
            }
        }
    );

    connect(
        searcher, &DesQ::Runner::AppSearch::updateDesktop, [ = ] ( QString url ) {
            DFL::XDG::DesktopFile desktopFile( QFileInfo( url ).fileName() );

            /** This desktop file is invalid. */
            if ( desktopFile.isValid() == false ) {
                /** The desktop was removed: So we will remove from our model */
                if ( QFile::exists( url ) == false ) {
                    if ( appsMap.contains( desktopFile.desktopName() ) ) {
                        appsModel->removeRow( appsMap[ desktopFile.desktopName() ]->row() );
                        appsMap.remove( desktopFile.desktopName() );
                    }
                }

                return;
            }

            if ( desktopFile.type() != DFL::XDG::DesktopFile::Type::Application ) {
                return;
            }

            /** This app is to be shown */
            if ( desktopFile.visible() ) {
                /** To be modified */
                if ( appsMap.contains( desktopFile.desktopName() ) ) {
                    QStandardItem *item = appsMap[ desktopFile.desktopName() ];
                    QModelIndex idx     = appsModel->indexFromItem( item );

                    appsModel->setData( idx, QIcon::fromTheme( desktopFile.icon() ), Qt::DisplayRole );
                    appsModel->setData( idx, desktopFile.name(),                     Qt::DecorationRole );
                    appsModel->setData( idx, desktopFile.genericName(),              Qt::UserRole + 1 );
                    appsModel->setData( idx, desktopFile.description(),              Qt::UserRole + 2 );
                    appsModel->setData( idx, desktopFile.desktopFileUrl(),           Qt::UserRole + 3 );

                    QString searchData = desktopFile.name();
                    searchData        += "\n" + desktopFile.genericName();
                    searchData        += "\n" + desktopFile.description();
                    searchData        += "\n" + desktopFile.desktopFileUrl();

                    /** We will use this column for search */
                    appsModel->setData( idx, searchData, Qt::UserRole + 4 );
                    appsModel->dataChanged( idx, idx );

                    // /** Update the proxy model */
                    // if ( currentMdl == appsModel ) {
                    //     QModelIndex proxyIdx = proxyModel->mapFromSource( idx );
                    //     appsModel->dataChanged( idx, idx );
                    // }
                }

                /** To be added */
                else {
                    QStandardItem *item = new QStandardItem();

                    item->setIcon( QIcon::fromTheme( desktopFile.icon() ) );
                    item->setText( desktopFile.name() );
                    item->setData( desktopFile.genericName(),    Qt::UserRole + 1 );
                    item->setData( desktopFile.description(),    Qt::UserRole + 2 );
                    item->setData( desktopFile.desktopFileUrl(), Qt::UserRole + 3 );

                    QString searchData = desktopFile.name();
                    searchData        += "\n" + desktopFile.genericName();
                    searchData        += "\n" + desktopFile.description();
                    searchData        += "\n" + desktopFile.desktopFileUrl();

                    /** We will use this column for search */
                    item->setData( searchData, Qt::UserRole + 4 );

                    appsModel->appendRow( item );
                    appsMap[ desktopFile.desktopName() ] = item;

                    qApp->processEvents();
                }
            }

            /** This app is not to be shown */
            else {
                if ( appsMap.contains( desktopFile.desktopName() ) ) {
                    appsModel->removeRow( appsMap[ desktopFile.desktopName() ]->row() );
                    appsMap.remove( desktopFile.desktopName() );
                }
            }
        }
    );

    /** Begin parsing our desktops and listing out cli apps */
    searcher->loadDatabase();

    connect(
        runSett, &DFL::Settings::settingChanged, [ = ] ( QString key, QVariant value ) mutable {
            /** Update the terminal preferences */
            if ( key.startsWith( "Terminal::" ) ) {
                terminal     = ( QString )runSett->value( "Terminal::AppExec" );
                termSwitches = runSett->value( "Terminal::ExecSwitches" );
            }

            else if ( key.contains( "MaxVisibleItems" ) ) {
                maxVisibleItems = value.toInt();
            }

            else if ( key.contains( "MaxVisibleItems" ) ) {
                maxVisibleItems = value.toInt();
            }

            else if ( key.contains( "FontSize" ) ) {
                QFont f( font() );
                f.setPixelSize( value.toInt() );

                cmdEdit->setFont( f );
                options->setFont( f );
            }

            else if ( key.contains( "IconSize" ) ) {
                mIconSize = value.toInt();
                setMinimumHeight( mIconSize + 20 );
                options->setIconSize( QSize( mIconSize, mIconSize ) );
            }
        }
    );

    connect(
        cmdEdit, &QLineEdit::textEdited, [ = ]() {
            if ( delayTimer->isActive() == false ) {
                delayTimer->start( 250, Qt::PreciseTimer, this );
            }
        }
    );

    connect(
        options, &QListView::clicked, [ = ]( QModelIndex idx ) {
            QStandardItem *item = currentMdl->itemFromIndex( proxyModel->mapToSource( idx ) );

            if ( item->text().startsWith( "/" ) ) {     // CLI App: Execute it using terminal
                launchApp( terminal, QStringList() << termSwitches << item->text(), true );
            }

            else {
                launchApp( item->data( Qt::UserRole + 3 ).toString(), {}, false );
            }

            history->store( cmdEdit->text() );

            hide();
        }
    );

    connect(
        options, &QListView::activated, [ = ]( QModelIndex idx ) {
            QStandardItem *item = currentMdl->itemFromIndex( proxyModel->mapToSource( idx ) );

            if ( item->text().startsWith( "/" ) ) { // CLI App: Execute it using terminal
                launchApp( terminal, QStringList() << termSwitches << item->text(), true );
            }

            else {
                launchApp( item->data( Qt::UserRole + 3 ).toString(), {}, false );
            }

            history->store( cmdEdit->text() );

            hide();
        }
    );

    connect(
        cmdEdit, &QLineEdit::returnPressed, [ = ]() {
            QString text     = cmdEdit->text();
            QStringList args = text.trimmed().split( QRegularExpression( "\\s+" ), Qt::SkipEmptyParts );

            /** Remove the leading ">" from the args. */
            if ( text.startsWith( ">" ) ) {
                args.removeFirst();
            }

            /**
             * User has given the full path.
             * First arg is the exec path
             */
            if ( text.startsWith( "/" ) ) {
                QString exec = args.takeFirst();

                if ( isCli( exec ) ) {
                    launchApp( exec, args, false );
                    hide();

                    history->store( text );
                }
            }

            /**
             * The first arg is the search term, so remove it.
             * We will be picking up the exec path from the model.
             */
            else {
                args.removeFirst();

                QModelIndex firstIndex = proxyModel->mapToSource( proxyModel->index( 0, 0 ) );
                QStandardItem *item    = currentMdl->itemFromIndex( firstIndex );

                if ( item ) {
                    /** CLI App: Execute it using terminal */
                    if ( item->text().startsWith( "/" ) ) {
                        args.push_front( item->text() );
                        launchApp( terminal, QStringList() << termSwitches << args.join( " " ), true );
                    }

                    else {
                        launchApp( item->data( Qt::UserRole + 3 ).toString(), { args }, false );
                    }

                    history->store( cmdEdit->text() );

                    hide();
                }
            }
        }
    );
}


void DesQ::Runner::UI::setupWindowProperties() {
    setWindowTitle( "DesQ Runner" );
    setWindowIcon( QIcon::fromTheme( "desq-runner" ) );

    setWindowFlags( Qt::Widget | Qt::FramelessWindowHint | Qt::BypassWindowManagerHint );
    setAttribute( Qt::WA_TranslucentBackground );

    setFixedWidth( qMax( 450, qApp->primaryScreen()->size().width() / 3 ) );
    setMinimumHeight( mIconSize + 20 );
    setMaximumHeight( mIconSize + 20 + 5 + mIconSize * maxVisibleItems );

    resize( width(), minimumHeight() );
}


void DesQ::Runner::UI::search( QString searchTerm ) {
    QString srch( searchTerm.trimmed().simplified() );

    /** Empty search string */
    if ( srch.isEmpty() ) {
        options->hide();
        resize( width(), minimumHeight() );
        cmdEdit->setPalette( qApp->palette() );

        return;
    }

    /** Run in terminal/shell, but nothing to run */
    else if ( srch.startsWith( ">" ) and QString( srch ).remove( 0, 2 ).trimmed().isEmpty() ) {
        options->hide();
        resize( width(), minimumHeight() );

        return;
    }

    /** CLI app */
    if ( srch.startsWith( ">" ) ) {
        if ( srch.startsWith( ">>" ) ) {
            options->hide();
            resize( width(), minimumHeight() );

            return;
        }

        else {
            srch.remove( 0, 2 );

            /** Our model is the CLI Apps Model */
            proxyModel->setSourceModel( clisModel );
            currentMdl = clisModel;

            proxyModel->setFilterRole( Qt::DisplayRole );
            proxyModel->setFilterWildcard( "*" + srch.split( QRegularExpression( "\\s+" ), Qt::SkipEmptyParts ).at( 0 ) + "*" );

            /** Nothing matches */
            if ( proxyModel->rowCount() == 0 ) {
                cmdEdit->setPalette( bad );
                options->hide();
                resize( width(), minimumHeight() );

                return;
            }

            cmdEdit->setPalette( qApp->palette() );
            options->show();

            /** Resize to show the correct number of items */
            resize( width(), minimumHeight() + 5 + (mIconSize + 12) * qMin( maxVisibleItems, proxyModel->rowCount() ) );
        }

        return;
    }

    /** Expected to be a gui app with full exec path */
    else if ( srch.trimmed().startsWith( "/" ) ) {
        QFileInfo execInfo( srch.split( QRegularExpression( "\\s+" ), Qt::SkipEmptyParts ).at( 0 ) );

        if ( execInfo.isFile() and execInfo.isExecutable() ) {
            cmdEdit->setPalette( qApp->palette() );
        }

        else {
            cmdEdit->setPalette( bad );
        }

        options->hide();
        resize( width(), minimumHeight() );

        return;
    }

    else {
        /** Our model is the Desktop Apps Model */
        proxyModel->setSourceModel( appsModel );
        currentMdl = appsModel;

        proxyModel->setFilterRole( Qt::UserRole + 4 );
        proxyModel->setFilterWildcard( "*" + srch.split( QRegularExpression( "\\s+" ), Qt::SkipEmptyParts ).at( 0 ) + "*" );

        if ( proxyModel->rowCount() == 0 ) {
            cmdEdit->setPalette( bad );
            options->hide();
            resize( width(), minimumHeight() );

            return;
        }

        cmdEdit->setPalette( qApp->palette() );
        options->show();
        resize( width(), minimumHeight() + 5 + (mIconSize + 12) * qMin( maxVisibleItems, proxyModel->rowCount() ) );

        return;
    }
}


void DesQ::Runner::UI::show() {
    QWidget::show();
    cmdEdit->setFocus();

    /** This is exclusively for wayfire - requires wayfire dbusqt plugin */
    if ( WQt::Utils::isWayland() ) {
        WQt::LayerShell::LayerType lyr = WQt::LayerShell::Overlay;
        cls = wlRegistry->layerShell()->getLayerSurface( windowHandle(), nullptr, lyr, "runner" );

        /** Size of our surface */
        cls->setSurfaceSize( size() );

        /** Don't be disturbed by anyone */
        cls->setExclusiveZone( -1 );

        /** We need exclusive keyboard interaction */
        cls->setKeyboardInteractivity( WQt::LayerSurface::Exclusive );

        /** Commit to our choices */
        cls->apply();
    }
}


void DesQ::Runner::UI::hide() {
    /** Delete the WQt::LayerSurface */
    delete cls;
    cls = nullptr;

    /** Now close the runner window */
    QWidget::hide();

    /** Clear the */
    cmdEdit->clear();

    /** Hide the options */
    options->hide();

    /** Resize the runner to default size */
    resize( width(), minimumHeight() );
}


void DesQ::Runner::UI::handleMessage( QString msg, int ) {
    if ( msg == "toggle" ) {
        if ( isVisible() ) {
            hide();
        }

        else {
            show();
        }
    }
}


void DesQ::Runner::UI::paintEvent( QPaintEvent *pEvent ) {
    QPainter painter( this );

    painter.fillRect( rect(), Qt::transparent );

    painter.setRenderHints( QPainter::Antialiasing );
    painter.setPen( QPen( QColor( 0x98, 0xFF, 0x98, 0x90 ), 2 ) );
    painter.setBrush( QColor( 0, 0, 0, 0xE6 ) );
    painter.drawRoundedRect( QRectF( rect() ).adjusted( 1, 1, -1, -1 ), 5, 5 );

    painter.end();

    QWidget::paintEvent( pEvent );
}


void DesQ::Runner::UI::keyPressEvent( QKeyEvent *kEvent ) {
    switch ( kEvent->key() ) {
        case Qt::Key_Escape: {
            /** Reset the position of the history to 0 */
            history->resetPosition();
            hide();
            break;
        }

        case Qt::Key_Up: {
            if ( cmdEdit->hasFocus() ) {
                QString srchTerm = history->older();
                cmdEdit->setText( srchTerm );
                search( srchTerm );
            }

            break;
        }

        case Qt::Key_Down: {
            if ( cmdEdit->hasFocus() ) {
                QString srchTerm = history->newer();

                if ( srchTerm.length() ) {
                    cmdEdit->setText( srchTerm );
                    search( srchTerm );
                }
            }

            break;
        }

        default: {
            QWidget::keyPressEvent( kEvent );
            break;
        }
    }
}


void DesQ::Runner::UI::timerEvent( QTimerEvent *tEvent ) {
    if ( delayTimer->timerId() == tEvent->timerId() ) {
        delayTimer->stop();
        search( cmdEdit->text() );
    }

    QWidget::timerEvent( tEvent );
}
