/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Runner (https://gitlab.com/DesQ/DesQUtils/Runner)
 * DesQ Runner is a simple app launcher for the the DesQ Project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include "Global.hpp"

namespace WQt {
    class LayerSurface;
}

namespace DesQ {
    namespace Runner {
        class UI;
        class AppSearch;
        class History;
    }
}

class OptionsDelegate : public QStyledItemDelegate {
    Q_OBJECT;

    public:
        void paint( QPainter *painter, const QStyleOptionViewItem& option, const QModelIndex& index ) const override;
        QSize sizeHint( const QStyleOptionViewItem& option, const QModelIndex& index ) const override;
};

class OptionsView : public QListView {
    Q_OBJECT;

    public:
        OptionsView( QWidget *parent );

    protected:
        void focusInEvent( QFocusEvent * );
};

class DesQ::Runner::UI : public QWidget {
    Q_OBJECT;

    public:
        UI();
        ~UI();

        void startTray();

    private:
        void createUI();
        void makeConnections();
        void setupWindowProperties();

        void search( QString );

        QLineEdit *cmdEdit;

        OptionsView *options;

        /** Item models */
        QStandardItemModel *appsModel;
        QStandardItemModel *clisModel;

        QStandardItemModel *currentMdl;

        /** SortFilter Proxy Model */
        QSortFilterProxyModel *proxyModel;

        DesQ::Runner::AppSearch *searcher;
        DesQ::Runner::History *history;

        /***/
        QHash<QString, QStandardItem *> appsMap;
        QHash<QString, QStandardItem *> clisMap;

        /** QLineEdit palette for bad search */
        QPalette bad;

        /** Terminal exec and switches */
        QString terminal;
        QStringList termSwitches;

        /** Icon size and Maximum Visible Items in options */
        int mIconSize;
        int maxVisibleItems;

        /** Search delay timer */
        QBasicTimer *delayTimer;

        /** WQt::LayerSurface object */
        WQt::LayerSurface *cls;

    public slots:
        void show();
        void hide();

        void handleMessage( QString, int );

    protected:
        void paintEvent( QPaintEvent * );
        void keyPressEvent( QKeyEvent * );

        void timerEvent( QTimerEvent * );
};
