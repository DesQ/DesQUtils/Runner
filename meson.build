project(
  'DesQ Runner',
  'c',
	'cpp',
	version: '0.0.9',
	license: 'GPLv3',
	meson_version: '>=0.59.0',
	default_options: [
		'cpp_std=c++17',
		'c_std=c11',
		'warning_level=2',
		'werror=false',
	],
)

add_global_arguments( '-DPROJECT_VERSION="v@0@"'.format( meson.project_version() ), language : 'cpp' )
add_project_link_arguments( ['-rdynamic','-fPIC'], language:'cpp' )

# If use_qt_version is 'auto', we will first search for Qt6.
# If all Qt6 packages are available, we'll use it. Otherwise,
# we'll switch to Qt5.
if get_option('use_qt_version') == 'auto'
	# Check if Qt6 is available
	QtDeps = dependency(
		'qt6',
		modules: [ 'Core', 'Gui', 'Widgets', 'Concurrent' ],
		required: false
	)

	WayQt      = dependency( 'wayqt-qt6', required: false )
	DesQCore   = dependency( 'desq-core-qt6', required: false )
	DesQGui    = dependency( 'desq-gui-qt6', required: false )

    DFIpc      = dependency( 'df6ipc', required: false )
    DFXdg      = dependency( 'df6xdg', required: false )
    DFApp      = dependency( 'df6application', required: false )
    DFUtils    = dependency( 'df6utils', required: false )
    DFInotify  = dependency( 'df6inotify', required: false )
    DFSettings = dependency( 'df6settings', required: false )

	Qt6_is_Found = QtDeps.found() and WayQt.found() and DFApp.found() and DesQCore.found() and DFSettings.found()
  Qt6_is_Found = Qt6_is_Found and DFIpc.found() and DFUtils.found() and DFXdg.found() and DFInotify.found()

	if Qt6_is_Found
		Qt = import( 'qt6' )
		message( 'Using Qt6' )

	else
		Qt = import( 'qt5' )
		QtDeps = dependency(
			'qt5',
			modules: [ 'Core', 'Gui', 'Widgets', 'Concurrent' ],
		)

		WayQt      = dependency( 'wayqt' )
		DesQCore   = dependency( 'desq-core' )
		DesQGui    = dependency( 'desq-gui' )

        DFIpc      = dependency( 'df5ipc' )
        DFXdg      = dependency( 'df5xdg' )
        DFApp      = dependency( 'df5application' )
        DFUtils    = dependency( 'df5utils' )
        DFInotify  = dependency( 'df5inotify' )
        DFSettings = dependency( 'df5settings' )
	endif

# User specifically wants to user Qt5
elif get_option('use_qt_version') == 'qt5'
	Qt = import( 'qt5' )

	QtDeps = dependency(
		'qt5',
		modules: [ 'Core', 'Gui', 'Widgets', 'Concurrent' ],
		required: true
	)

	WayQt      = dependency( 'wayqt' )
	DesQCore   = dependency( 'desq-core' )
	DesQGui    = dependency( 'desq-gui' )

    DFIpc      = dependency( 'df5ipc' )
    DFXdg      = dependency( 'df5xdg' )
    DFApp      = dependency( 'df5application' )
    DFUtils    = dependency( 'df5utils' )
    DFInotify  = dependency( 'df5inotify' )
    DFSettings = dependency( 'df5settings' )

# User specifically wants to user Qt6
elif get_option('use_qt_version') == 'qt6'
	Qt = import( 'qt6' )

	QtDeps = dependency(
		'qt6',
		modules: [ 'Core', 'Gui', 'Widgets', 'Concurrent' ],
	)

	WayQt      = dependency( 'wayqt-qt6' )
	DesQCore   = dependency( 'desq-core-qt6' )
	DesQGui    = dependency( 'desq-gui-qt6' )

    DFIpc      = dependency( 'df6ipc' )
    DFXdg      = dependency( 'df6xdg' )
    DFApp      = dependency( 'df6application' )
    DFUtils    = dependency( 'df6utils' )
    DFInotify  = dependency( 'df6inotify' )
    DFSettings = dependency( 'df6settings' )
endif

Deps = [ QtDeps, DesQCore, DesQGui, WayQt, DFIpc, DFApp, DFUtils, DFSettings, DFXdg, DFInotify ]

Includes = [
    'Core',
    'UI'
]

Headers = [
    'Global.hpp',
    'Core/AppSearch.hpp',
    'Core/History.hpp',
    'Core/Utils.hpp',
    'UI/Runner.hpp'
]

Sources = [
    'Main.cpp',
    'Core/AppSearch.cpp',
    'Core/History.cpp',
    'Core/Utils.cpp',
    'UI/Runner.cpp'
]

Moc = Qt.compile_moc(
	headers : Headers,
	dependencies: QtDeps
)

Qrc = Qt.compile_resources(
	name: 'desqrunner_rcc',
	sources: 'icons/icons.qrc'
)

desqrunner = executable(
  'desq-runner', [ Sources, Moc, Qrc ],
  dependencies: Deps,
  include_directories: Includes,
  install: true,
  install_dir: join_paths( get_option( 'libdir' ), get_option( 'libexecdir' ), 'desq' )
)

install_data(
    'Runner.conf',
    install_dir: join_paths( get_option( 'datadir' ), 'desq', 'configs' ),
)

install_data(
    'icons/desq-runner.svg',
    install_dir: join_paths( get_option( 'datadir' ), 'icons', 'hicolor', 'scalable', 'apps' ),
)

install_data(
    'icons/desq-runner.png',
    install_dir: join_paths( get_option( 'datadir' ), 'icons', 'hicolor', '512x512', 'apps' ),
)

summary = [
	'',
	'------------------',
	'DesQ Runner  @0@'.format( meson.project_version() ),
	'------------------',
	''
]
message( '\n'.join( summary ) )
